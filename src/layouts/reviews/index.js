

import { useState, useEffect, useMemo } from "react";
// @mui material components
import Grid from "@mui/material/Grid";
import Icon from "@mui/material/Icon";

// Soft UI Dashboard React components
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";

// Soft UI Dashboard React examples
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
import MiniStatisticsCard from "examples/Cards/StatisticsCards/MiniStatisticsCard";
import ReportsBarChart from "examples/Charts/BarCharts/ReportsBarChart";
import GradientLineChart from "examples/Charts/LineCharts/GradientLineChart";

// Soft UI Dashboard React base styles
import typography from "assets/theme/base/typography";

// Dashboard layout components
import BuildByDevelopers from "layouts/dashboard/components/BuildByDevelopers";
import WorkWithTheRockets from "layouts/dashboard/components/WorkWithTheRockets";
import Projects from "layouts/dashboard/components/Projects";
import OrderOverview from "layouts/dashboard/components/OrderOverview";

// Data
import reportsBarChartData from "layouts/dashboard/data/reportsBarChartData";
import gradientLineChartData from "layouts/dashboard/data/gradientLineChartData";
// Soft UI Dashboard React contexts
import { useSoftUIController, setMiniSidenav, setOpenConfigurator } from "context";
import routes from "routes";
// Images
import brand from "assets/images/courtena-logo-black-nobg.png";
import Sidenav from "examples/Sidenav";
import Configurator from "examples/Configurator";
import { useNavigate } from "react-router-dom";
import courtena from "api/courtena";
function Reviews() {
  const { size } = typography;
  const { chart, items } = reportsBarChartData;
  const [counts,setCounts] = useState()
  const [controller, dispatch] = useSoftUIController();
  const { miniSidenav, direction, layout, openConfigurator, sidenavColor } = controller;
  const [onMouseEnter, setOnMouseEnter] = useState(false);
  let navigate = useNavigate();
  // Open sidenav when mouse enter on mini sidenav
  const handleOnMouseEnter = () => {
    if (miniSidenav && !onMouseEnter) {
      setMiniSidenav(dispatch, false);
      setOnMouseEnter(true);
    }
  };

  const checkReviewsCount = async() => {
    var adminInfoString = localStorage.getItem("admin")
    var adminInfo = JSON.parse(adminInfoString)
    await courtena.get("/admin/get-reviews-counts/",{
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': adminInfo.token
    }}).then((res) => {
      console.log(res.data)
      setCounts(res.data.result)
    }).catch(err => {
      console.log(err)
    })
  }

  useEffect(() => {
    checkReviewsCount()
  },[])

  // Close sidenav when mouse leave mini sidenav
  const handleOnMouseLeave = () => {
    if (onMouseEnter) {
      setMiniSidenav(dispatch, true);
      setOnMouseEnter(false);
    }
  };
  const handleConfiguratorOpen = () => setOpenConfigurator(dispatch, !openConfigurator);
  const configsButton = (
    <SoftBox
      display="flex"
      justifyContent="center"
      alignItems="center"
      width="3.5rem"
      height="3.5rem"
      bgColor="white"
      shadow="sm"
      borderRadius="50%"
      position="fixed"
      right="2rem"
      bottom="2rem"
      zIndex={99}
      color="dark"
      sx={{ cursor: "pointer" }}
      onClick={handleConfiguratorOpen}
    >
      <Icon fontSize="default" color="inherit">
        settings
      </Icon>
    </SoftBox>
  );
  return (
    <DashboardLayout>
      
      <DashboardNavbar />
      <SoftBox py={3}>
        <SoftBox mb={3}>
          <Grid container spacing={3}>

            <Grid className="cursor-pointer" onClick={() => {navigate("/reviews/venues")}} item xs={12} sm={6} xl={3}>
              <MiniStatisticsCard
                title={{ text: "Venues In Review" }}
                count={counts?.venue_review_count}
                percentage={{ color: "", text: "" }}
                icon={{ color: "info", component: "sports_tennis" }}
                
              />
            </Grid>
            <Grid className="cursor-pointer" onClick={() => {navigate("/reviews/courts")}} item xs={12} sm={6} xl={3}>
              <MiniStatisticsCard
                title={{ text: "Courts in Review" }}
                count={counts?.court_review_count}
                percentage={{ color: "", text: "" }}
                icon={{ color: "info", component: "sports_tennis" }}
                
              />
            </Grid>

          </Grid>
        </SoftBox>

      </SoftBox>
      <Footer />
    </DashboardLayout>
  );
}

export default Reviews;
