import axios from "axios"

export default axios.create({
    // baseURL:"https://novamdigital.com/api/v1"
    baseURL:"https://api.courtena.com/api"
    // baseURL:"http://localhost:8800/api"
});